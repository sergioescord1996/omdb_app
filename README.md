# OMDB App iOS

## Summary

OMDB App es un pequeño proyecto para visualizar un listado de películas y sus detalles, con alguna funcionalidad extra como copiar texto, compartir la web de la película si esta tuviera con otras apps y ver en pantalla completa como descargar el poster de la película. Para la realización nos hemos servido de la API externa omdbapi.com. Se ha utilizado la versión 12.4 de Xcode y Swift 5. Esta aplicación está desarrollada para iOS 14. Se han usado diferentes patrones, como el de delegados para comunicación entre clase, los completions para las tareas asíncronas. Para las vistas de las escenas se han usado storyboards pero también se ha usado un .xib para un controlador auxiliar. Se han usado extensiones para facilitar el trabajo con la imagen, así como archivos compartidos para una mejor organización del código en general. Se ha configura el ATS (App Transport Security) para aceptar el dominio del la API bajo HTTP sin certficiado SSL.

## Architecture

Para este proyecto se ha optado por la arquitectura MVP (Modelo Vista Presentador) ya que al ser un proyecto pequeño era la más conveniente por complejidad y tiempo. Usar alguna arquitectura más elaborada habría añadido más complejidad al desarrollo y no habría sacado a relucir sus ventajas.
 
## Libraries

No se ha usado ningun librería de terceros en este proyecto ya que por complejidad las ventajas de usarlas no eran significativas.
