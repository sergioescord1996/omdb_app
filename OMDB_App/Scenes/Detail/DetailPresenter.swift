//
//  DetailPresenter.swift
//  OMDB_App
//
//  Created by Sergio Escalante Ordonez on 30/1/21.
//

import Foundation

protocol DetailPresenterDelegate: class {
    func attach(_ view: DetailViewControllerDelegate)
    func checkShareButton() -> Bool?
    func shareButtonPressed()
    func imagePressed()
}

class DetailPresenter: DetailPresenterDelegate {
    
    // MARK: - Variables
    weak var view: DetailViewControllerDelegate?
    var movieID: String?
    var model: DetailModel?
    
    // MARK: - Delegate methods
    func attach(_ view: DetailViewControllerDelegate) {
        self.view = view
        
        guard let movieID = self.movieID else {
            self.view?.showError("Ha ocurrido un error inesperado")
            return
        }
        
        NetworkManager.shared.getMovieDetail(for: movieID) { (result) in
            switch result {
            case let .failure(error):
                self.view?.showError(error.message)
            case let .success(data):
                self.model = data
                
                if let model = self.model {
                    self.view?.setupUI(model)
                }
            }
        }
    }
    
    func checkShareButton() -> Bool? {
        return model?.isValidWeb()
    }
    
    func shareButtonPressed() {
        
        guard let web = model?.getWeb(), let urlWeb = URL(string: web) else {
            self.view?.showError("Ha ocurrido un error inesperado")
            return
        }
        
        self.view?.showShareView(urlWeb)
    }
    
    func imagePressed() {
        self.view?.showFullScreenImage()
    }
}
