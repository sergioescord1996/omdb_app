//
//  DetailModel.swift
//  OMDB_App
//
//  Created by Sergio Escalante Ordonez on 30/1/21.
//

import Foundation

struct DetailModel: Codable {
    let imageUrl: String
    let title: String
    let releaseDate: String
    let duration: String
    let genres: String
    let sinopsis: String
    let web: String
    
    enum CodingKeys: String, CodingKey {
        case imageUrl = "Poster"
        case title = "Title"
        case releaseDate = "Released"
        case duration = "Runtime"
        case genres = "Genre"
        case sinopsis = "Plot"
        case web = "Website"
    }
    
    func isValidWeb() -> Bool {
        return web != "N/A" && !web.isEmpty
    }
    
    func getWeb() -> String {
        return web
    }
}
