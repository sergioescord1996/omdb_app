//
//  DetailViewController.swift
//  OMDB_App
//
//  Created by Sergio Escalante Ordonez on 30/1/21.
//

import UIKit

protocol DetailViewControllerDelegate: class {
    func showError(_ message: String)
    func setupUI(_ movie: DetailModel)
    func showShareView(_ web: URL)
    func showFullScreenImage()
}

class DetailViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var sinopsis: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var genres: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var web: UILabel!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var webLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    
    // MARK: - Constants
    
    let presenter = DetailPresenter()
    
    // MARK: - Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.attach(self)
    }
    
    // MARK: - IBActions Methods
    @IBAction func shareButtonPressed(_ sender: UIButton) {
        presenter.shareButtonPressed()
    }
    
    // MARK: - Custom Methods
    
    @objc func tapImageHandler(_ tapGestureRecognizer: UITapGestureRecognizer) {
        presenter.imagePressed()
    }
}


// MARK: - Extension DetailViewControllerDelegate

extension DetailViewController: DetailViewControllerDelegate {
    func showError(_ message: String) {
        DispatchQueue.main.async { [weak self] in
            
            let alert = UIAlertController(title: "Ooops!", message: message, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (action) in
                alert.dismiss(animated: true) {
                    self?.navigationController?.popViewController(animated: true)
                }
            }))
            
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    func setupUI(_ movie: DetailModel) {
        
        DispatchQueue.main.async { [weak self] in
        
            self?.loading.stopAnimating()
            
            self?.navigationController?.title = movie.title
            
            self?.movieImage.imageFromServerURL(with: movie.imageUrl) {
                let tap = UITapGestureRecognizer(target: self, action: #selector(self?.tapImageHandler))
                self?.movieImage.addGestureRecognizer(tap)
                self?.movieImage.isUserInteractionEnabled = true
            }

            self?.sinopsis.text = movie.sinopsis
            
            self?.durationLabel.isHidden = false
            self?.duration.text = movie.duration
            
            self?.genresLabel.isHidden = false
            self?.genres.text = movie.genres
            
            self?.releaseDateLabel.isHidden = false
            self?.releaseDate.text = movie.releaseDate
            
            self?.webLabel.isHidden = false
            self?.web.text = movie.web
            
            self?.shareButton.isHidden = false
            self?.shareButton.isEnabled = self?.presenter.checkShareButton() ?? false
        }
    }
    
    func showShareView(_ web: URL) {
        
        let items = [web]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        
        DispatchQueue.main.async { [weak self] in
            self?.present(ac, animated: true)
        }
    }
    
    func showFullScreenImage() {
        let view = FullScreenImage(nibName: Constants.ViewControllers.FullScreenImage, bundle: nil)
        view.image = movieImage.image
        self.present(view, animated: true, completion: nil)
    }
}
