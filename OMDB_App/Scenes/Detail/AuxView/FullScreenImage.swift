//
//  FullScreenImage.swift
//  OMDB_App
//
//  Created by Sergio Escalante Ordonez on 31/1/21.
//

import UIKit

class FullScreenImage: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    
    // MARK: - Variables
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = self.image
    }
    
    // MARK: - IBActions
    
    @IBAction func shareButtonPressed(_ sender: UIButton) {
        
        guard let image = self.image else { return }
        
        let items = [image]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        
        DispatchQueue.main.async { [weak self] in
            self?.present(ac, animated: true)
        }
    }
}
