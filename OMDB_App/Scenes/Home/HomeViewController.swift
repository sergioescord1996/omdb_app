//
//  HomeViewController.swift
//  OMDB_App
//
//  Created by Sergio Escalante Ordonez on 29/1/21.
//

import UIKit

protocol HomeViewControllerDelegate: class {
    func setupUI()
    func showError(_ message: String)
    func reloadData()
    func navigateToDetail(with id: String)
}

class HomeViewController: UITableViewController {

    // MARK: - IBOutlets
    
    // MARK: - Constants
    var presenter: HomePresenterDelegate = HomePresenter()

    // MARK: - Variables
    
    lazy var searchController: UISearchController = ({
        
        let controller = UISearchController(searchResultsController: nil)
        controller.hidesNavigationBarDuringPresentation = true
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchBar.sizeToFit()
        controller.searchBar.barStyle = .default
        controller.searchBar.backgroundColor = .clear
        controller.searchBar.placeholder = "Buscar una película"
        
        return controller
    })()

    // MARK: - Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.attach(self)
    }
    
    // MARK: - IBActions
    
    // MARK: - Datasource Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getNumberOfMovies()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCells.MovieCell, for: indexPath) as? MovieCell else {
            return UITableViewCell()
        }
        
        let movie = presenter.getMovie(for: indexPath.row)
        cell.setupUI(movie)
        
        return cell
    }
    
    // MARK: - Delegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter.selectMovie(at: indexPath.row)
    }
    
    // MARK: - Configure Methods
    
    private func configureTableView() {
        let movieCell = UINib(nibName: Constants.TableViewCells.MovieCell, bundle: nil)
        tableView.register(movieCell, forCellReuseIdentifier: Constants.TableViewCells.MovieCell)
    }
    
    private func configureSearchBarController() {
        let searchBar = searchController.searchBar
        searchController.searchBar.delegate = self
        tableView.tableHeaderView = searchBar
    }
}

// MARK: - Extension HomeViewDelegate

extension HomeViewController: HomeViewControllerDelegate {
    func setupUI() {
        configureTableView()
        configureSearchBarController()
    }
    
    func showError(_ message: String) {
        
        DispatchQueue.main.async { [weak self] in
            
            let alert = UIAlertController(title: "Ooops!", message: message, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            }))
            
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    func reloadData() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    func navigateToDetail(with id: String) {
        let storyboard = UIStoryboard(name: Constants.Storyboards.Detail, bundle: nil)
        
        guard let viewController = storyboard.instantiateViewController(withIdentifier: Constants.ViewControllers.DetailViewController) as? DetailViewController else {
            return
        }
        
        viewController.presenter.movieID = id
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

// MARK: - Searchbar Delegate Methods

extension HomeViewController: UISearchBarDelegate {

    // Debido al comportamiento de la api cuando hay demasiados resultados no envía el listado,
    // omitiré actualizar dicho listado en cada cambio ya que se produce un comportamiento extraño
    // haciendo así que la experiencia de usuario no sea adecuada

//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        presenter.loadData(searchText)
//    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            presenter.loadData(text)
        }
        searchController.isActive = false
    }
}
