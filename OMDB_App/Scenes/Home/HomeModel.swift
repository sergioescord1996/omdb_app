//
//  HomeModel.swift
//  OMDB_App
//
//  Created by Sergio Escalante Ordonez on 29/1/21.
//

struct HomeModel: Codable {
    let movies: [Movie]
    
    enum CodingKeys: String, CodingKey {
        case movies = "Search"
    }
    
    init() {
        movies = []
    }
    
    func numberOfMovies() -> Int {
        return movies.count
    }
    
    func getMovie(for index: Int) -> Movie {
        return movies[index]
    }
    
    func getIDMovie(at index: Int) -> String {
        return movies[index].id
    }
}

struct Movie: Codable {
    
    let id: String
    let title: String
    let year: String
    let imageUrl: String
    
    enum CodingKeys: String, CodingKey {
        case id = "imdbID"
        case title = "Title"
        case year = "Year"
        case imageUrl = "Poster"
    }
}
