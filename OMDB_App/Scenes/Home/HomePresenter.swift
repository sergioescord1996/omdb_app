//
//  HomePresenter.swift
//  OMDB_App
//
//  Created by Sergio Escalante Ordonez on 29/1/21.
//

import Foundation

protocol HomePresenterDelegate: class {
    func attach(_ view: HomeViewControllerDelegate)
    func loadData(_ searchText: String)
    func getNumberOfMovies() -> Int
    func getMovie(for index: Int) -> Movie
    func selectMovie(at index: Int)
}

class HomePresenter: HomePresenterDelegate {
    
    // MARK: - Variables
    weak var view: HomeViewControllerDelegate?
    var model: HomeModel = HomeModel()
    
    // MARK: - Delegate Methods
    
    func attach(_ view: HomeViewControllerDelegate) {
        self.view = view
        view.setupUI()
    }
    
    func loadData(_ searchText: String) {
        NetworkManager.shared.getMovieList(for: searchText) { result in
            switch result {
            case let .failure(error):
                self.view?.showError(error.message)
            case let .success(data):
                self.model = data
                self.view?.reloadData()
            }
        }
    }
    
    func getNumberOfMovies() -> Int {
        return model.numberOfMovies()
    }
    
    func getMovie(for index: Int) -> Movie {
        return model.getMovie(for: index)
    }
    
    func selectMovie(at index: Int) {
        let id = model.getIDMovie(at: index)
        view?.navigateToDetail(with: id)
    }
}
