//
//  MovieCell.swift
//  OMDB_App
//
//  Created by Sergio Escalante Ordonez on 29/1/21.
//

import UIKit

class MovieCell: UITableViewCell {

    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupUI(_ model: Movie) {
        titleLabel.text = "\(model.title) (\(model.year))"
        movieImage.imageFromServerURL(with: model.imageUrl, completion: nil)
    }
    
}
