//
//  CustomError.swift
//  OMDB_App
//
//  Created by Sergio Escalante Ordonez on 29/1/21.
//

struct CustomError: Codable, Error {
    var message: String
    var response: String
    
    enum CodingKeys: String, CodingKey {
        case message = "Error"
        case response = "Response"
    }
}
