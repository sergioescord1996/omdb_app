//
//  UIImage+Extensions.swift
//  OMDB_App
//
//  Created by Sergio Escalante Ordonez on 29/1/21.
//

import UIKit

extension UIImageView {
    func imageFromServerURL(with urlString: String,
                            default placeholderImage: UIImage = UIImage(systemName: "film.fill")!,
                            completion: (() -> ())?) {
        
        self.image = placeholderImage
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                return
            }
            
            DispatchQueue.main.async {
                guard let data = data else { return }
                let image = UIImage(data: data)
                self.image = image
                completion?()
            }
        }.resume()
    }
}
