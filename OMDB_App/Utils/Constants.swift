//
//  Constants.swift
//  OMDB_App
//
//  Created by Sergio Escalante Ordonez on 29/1/21.
//

struct Constants {
    
    static let baseUrl = "http://www.omdbapi.com/?apikey=\(apiKey)&type=movie&"
    
    static let apiKey = "<API-KEY>"
    
    struct ApiParameters {
        static let MovieList = "s="
        static let MovieDetail = "i="
    }
    
    struct TableViewCells {
        static let MovieCell = "MovieCell"
    }
    
    struct Storyboards {
        static let Detail = "Detail"
    }
    
    struct ViewControllers {
        static let DetailViewController = "DetailViewController"
        static let FullScreenImage = "FullScreenImage"
    }
}
