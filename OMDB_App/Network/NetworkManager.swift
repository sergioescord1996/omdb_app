//
//  NetworkManager.swift
//  OMDB_App
//
//  Created by Sergio Escalante Ordonez on 29/1/21.
//

import Foundation

struct NetworkManager {
    
    static let shared = NetworkManager()
    
    typealias ResultBlock<T> = (Result <T, CustomError>) -> Void
    
    func getMovieList (for title: String, completion: @escaping ResultBlock<HomeModel>) {
        var urlString = Constants.baseUrl + Constants.ApiParameters.MovieList + title
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        perfomRequest(with: urlString, of: HomeModel.self, completion: completion)
    }
    
    func getMovieDetail(for id: String, completion: @escaping ResultBlock<DetailModel>) {
        let urlString = Constants.baseUrl + Constants.ApiParameters.MovieDetail + id
        perfomRequest(with: urlString, of: DetailModel.self, completion: completion)
    }
    
    private func perfomRequest<T: Decodable> (with urlString: String, of type: T.Type, completion: @escaping ResultBlock<T>) {
        guard let url = URL(string: urlString) else {
            completion(.failure(CustomError(message: "Ha ocurrido un error desconocido", response: "false")))
            return
        }
        
        let session = URLSession(configuration: .default)
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            let defaultError = CustomError(message: "Ha ocurrido un error desconocido", response: "false")
            
            if let error = error {
                let customError = CustomError(message: error.localizedDescription, response: "false")
                completion(.failure(customError))
            }
            
            guard let safeData = data else {
                completion(.failure(defaultError))
                return
            }
            
            if let parseData = parseJSON(of: T.self, from: safeData) {
                completion(.success(parseData))
            } else if let error = parseJSON(of: CustomError.self, from: safeData) {
                completion(.failure(error))
            } else {
                completion(.failure(defaultError))
            }
        }
        
        task.resume()
    }
    
    private func parseJSON<T: Decodable> (of type: T.Type, from data: Data) -> T? {
        let decoder = JSONDecoder()
        
        do {
            let data = try decoder.decode(T.self, from: data)
            return data
        } catch {
            return nil
        }
    }
}
